<?php
session_start();

// Correct credentials
$correctUsername = 'johnsmith@gmail.com';
$correctPassword = '1234';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $email = $_POST['email'];
  $password = $_POST['password'];

  if ($email === $correctUsername && $password === $correctPassword) {
    // Set session variables
    $_SESSION['email'] = $email;
    $_SESSION['loggedIn'] = true;
    header('Location: dashboard.php');
    exit();
  } else {
    //Pag mali input ni user
    echo "Incorrect email or password. Please try again.";
  }
}
?>
