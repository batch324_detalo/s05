<?php
session_start();

if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] !== true) {
  header('Location: login.php');
  exit();
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Logged In</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
  <div class="container">
    <!-- Once success yung log in, greetings -->
    <h2>Hello, <?php echo $_SESSION['email']; ?></h2>
    <form action="logout.php" method="post">
      <input type="submit" value="Logout">
    </form>
  </div>
</body>
</html>
